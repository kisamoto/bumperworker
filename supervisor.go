package bumperworker

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

var logger *log.Logger

type supervisor struct {
	db       *bdb.BumperDB
	quitChan chan bool
	osSignal chan os.Signal
}

func (s *supervisor) Start(concurrency string) {
	signal.Notify(s.osSignal,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	// Make some channels to dump locations and messages on
	locChan := make(chan bdb.Location)
	msgChan := make(chan bdb.Message)

	c, err := strconv.ParseInt(concurrency, 10, 64)
	if err != nil {
		logger.Printf("Unable to extract concurrency from '%v'", concurrency)
		logger.Printf("Defaulting to: %v", defaultConcurrency)
		c = defaultConcurrency
	}

	// Only have one reading from queue at the moment
	go s.readFromQueues(locChan, msgChan)

	// Start concurrently
	var i int64
	for i = 1; i <= c; i++ {
		locWorker := newLocWorker(s.db, i, s.quitChan, locChan)
		go locWorker.start()

		msgWorker := newMsgWorker(s.db, i, s.quitChan, msgChan)
		go msgWorker.start()
	}

	for {
		select {
		case <-s.osSignal:
			s.Stop()
			os.Exit(0)
		}
	}
}

func (s *supervisor) Stop() {
	logger.Println("Stopping workers")
	s.quitChan <- true
}

func (self *supervisor) readFromQueues(locChan chan bdb.Location, msgChan chan bdb.Message) {
	for {
		loc, err := self.db.LocPopPushProcessing()
		if err == nil && &loc != nil {
			locChan <- loc
		} else {
			//			fmt.Printf("Nothing on queue, waiting...")
		}
		msg, err := self.db.MsgPopPushProcessing()
		if err == nil && &msg != nil {
			msgChan <- msg
		}
	}
}

func NewSupervisor(
	redisProtocol, redisHost, redisLocQueue, redisMsgQueue,
	postgresURI, postgresUserSchema, postgresAnalyticsSchema string) (s *supervisor) {

	db := bdb.NewBumperDB()
	db.SetRedis(redisProtocol, redisHost, redisLocQueue, redisMsgQueue)
	db.SetupPostgres(postgresURI, postgresUserSchema, postgresAnalyticsSchema)

	quitChan := make(chan bool, 1)
	sigc := make(chan os.Signal, 1)

	logger = log.New(os.Stderr, "", log.LstdFlags)

	return &supervisor{db: db, quitChan: quitChan, osSignal: sigc}
}
