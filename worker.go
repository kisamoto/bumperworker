package bumperworker

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
)

type msgWorker struct {
	db       *bdb.BumperDB
	workerId int64
	quitChan chan bool
	workChan chan bdb.Message
}

func (self *msgWorker) start() {
	var msg bdb.Message
	logger.Printf("msgWorker.start: Starting message worker: %v", self.workerId)
	for {
		select {
		case <-self.quitChan:
			logger.Printf("msgWorker.start: Stopping worker: %v", self.workerId)
			self.quitChan <- true
			return
		case msg = <-self.workChan:
			logger.Printf("msgWorker.start: Message received: %v", msg)
		}
	}
}

type locWorker struct {
	db       *bdb.BumperDB
	workerId int64
	quitChan chan bool
	workChan chan bdb.Location
}

func (self *locWorker) start() {
	var loc bdb.Location
	var err error
	logger.Printf("locWorker.start: Starting location worker: %v", self.workerId)
	for {
		select {
		case <-self.quitChan:
			logger.Printf("locWalker.start: Stopping worker: %v", self.workerId)
			self.quitChan <- true
			return
		case loc = <-self.workChan:
			logger.Printf("locWorker.start: Message received: %v", loc)
			err = self.db.AddLocationPoint(loc)
			if err != nil {
				logger.Printf("locWorker.start: Error saving location, requeuing.")
				err = self.db.LocRemoveProcessingRequeue(loc)
				if err != nil {
					logger.Printf("locWorker.start: Unable to requeue")
					logger.Println(err)
				}
			} else {
				_, err = self.db.RemoveLocProcessing(loc)
				if err != nil {
					logger.Printf("locWorker.start: Unable to remove from processing queue.")
					logger.Println(err)
				}
			}
		}
	}
}

func newMsgWorker(db *bdb.BumperDB, workerId int64, quitChan chan bool, workChan chan bdb.Message) (w *msgWorker) {
	return &msgWorker{db: db, workerId: workerId, quitChan: quitChan, workChan: workChan}
}

func newLocWorker(db *bdb.BumperDB, workerId int64, quitChan chan bool, workChan chan bdb.Location) (w *locWorker) {
	return &locWorker{db: db, workerId: workerId, quitChan: quitChan, workChan: workChan}
}
