package main

import (
	bw "bitbucket.org/kisamoto/bumperworker"
	"os"
)

func GetenvWithDefault(envVar, defaultValue string) (envVarValue string) {
	envVarValue = os.Getenv(envVar)
	if envVarValue == "" {
		envVarValue = defaultValue
	}
	return
}

func GetDBConfFromEnvVars() (redisProtocol, redisHost, redisLocQueue, redisMsgQueue,
	postgresURI, postgresUserSchema, postgresAnalyticsSchema string) {
	// Redis protocol
	redisProtocol = GetenvWithDefault("REDIS_PROTOCOL", "tcp")
	// Redis host
	redisHost = GetenvWithDefault("REDIS_HOST", ":6379")
	// Redis Location Queue
	redisLocQueue = GetenvWithDefault("REDIS_LOC_QUEUE", "locations")
	// Redis Message Queue
	redisMsgQueue = GetenvWithDefault("REDIS_MSG_QUEUE", "messages")

	// Postgres URI
	postgresURI = GetenvWithDefault("POSTGRES_URI", "postgres://bumper:bumper@localhost/bumper?sslmode=disable")
	// Postgres User Schema
	postgresUserSchema = GetenvWithDefault("POSTGRES_USER_SCHEMA", "usr")
	// Postgres Analytics Schema
	postgresAnalyticsSchema = GetenvWithDefault("POSTGRES_ANALYTICS_SCHEMA", "analytics")
	return
}

func GetConcurrencyFromEnvVars() (concurrency string) {
	concurrency = GetenvWithDefault("WORKER_CONCURRENCY", "100")
	return
}

func main() {
	worker := bw.NewSupervisor(GetDBConfFromEnvVars())
	worker.Start(GetConcurrencyFromEnvVars())
}
